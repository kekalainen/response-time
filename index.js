const readline = require('readline')
const request = require('request')
const ora = require('ora')
const cliSpinners = require('cli-spinners')
const spinner = ora()

function askQuestion(query) {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    })

    return new Promise(resolve => rl.question(query + '\n> ', ans => {
        rl.close()
        resolve(ans)
    }))
}

function getRequest(url, current, total) {
    return new Promise(resolve =>
        request.get({
            url: url || 'http://google.com',
            time: true
        }, (err, response) => {
            spinner.text = "Sending request " + (current + 1) + "/" + total + ". Last request took " + response.elapsedTime + " milliseconds."
            resolve(response.elapsedTime)
        }
    ))
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

const main = async () => {
    const url = await askQuestion('Enter the URL of the site to measure response time for (default: "http://google.com")')
    let amt = parseInt(await askQuestion("Amount of requests to send (default: 10)"))
    if (isNaN(amt)) { amt = 10 }
    const delay = await askQuestion("Request delay in milliseconds (default: 100)")
    if (isNaN(delay)) { delay = 100 }

    console.log("")
    spinner.spinner = cliSpinners.dots
    spinner.start()

    let responseTimes = [];
    for (var i = 0; i < amt; i++) {
        responseTimes.push(await getRequest(url, i, amt))
        await sleep(delay)
    }
    
    spinner.stopAndPersist({
        symbol: '✔'
    })

    let sum = 0
    for (var i = 0; i < responseTimes.length; i++) {
        sum += responseTimes[i]
    }

    console.log("\nAverage response time: " + sum / responseTimes.length + " milliseconds")
}

main()